import { Module, DynamicModule } from '@nestjs/common';

import { BootServiceInterface } from '@binance-test/boot';
import { LoggerGlobalModule } from '@binance-test/logger';
import { ConfigGlobalModule } from '@binance-test/config';

import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({})
export class AppModule {

	public static forRoot(bootService: BootServiceInterface): DynamicModule {
		return {
			module: AppModule,
			imports: [
				ConfigGlobalModule.forRoot(bootService),
				LoggerGlobalModule.forRoot(bootService),
			],
			controllers: [
				AppController,
			],
			providers: [
				AppService,
			],
		};
	}
}
