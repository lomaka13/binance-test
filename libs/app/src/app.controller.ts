import { Controller, Get } from '@nestjs/common';
import { ApiExcludeEndpoint } from '@nestjs/swagger';

import { API_VERSION } from '@binance-test/api';

import { AppService } from './app.service';

@Controller()
export class AppController {
	constructor(protected appService: AppService) {}

	@Get()
	@ApiExcludeEndpoint()
	getBase(): any {
		return;
	}

	@Get(['info', 'version', 'health-check'])
	@ApiExcludeEndpoint()
	getInfo(): any {
		return this.appService.getInfo();
	}

	@Get(`${API_VERSION.API}/version-check`)
	checkVersion(): string {
		return this.appService.getVersion();
	}
}
