import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common';

import { HttpStatus, RequestHeader } from '@binance-test/api';
import {
	AppException,
	BadParamsException,
	AuthException,
	FatalException,
	NotExistsException,
	ExceptionInfo,
	ServiceUnavailableException,
	UnprocessableException,
	NotImplementedException,
	RecordChangedException,
	ErrorCode,
	ConflictException,
} from '@binance-test/exceptions';
import { unknownHasProp } from '@binance-test/fixtures';
import { LogMessage, Logger, LogType } from '@binance-test/logger';

import { REQUEST_LOGGER } from './app.constants';
import { Request, Response } from './app.types';
import { addHeaders, getRequestInfo } from './util';

type AppExceptionInfo = ExceptionInfo & { status: number; headers?: any };

/**
 * Error filter class catches all errors and forms response
 * regarding error/exception type.
 */
@Catch()
export class ErrorFilter implements ExceptionFilter {
	constructor(private logger: Logger) {}

	catch(exception: Error, host: ArgumentsHost): Error | void {
		const ctx = host.switchToHttp();
		const request = ctx.getRequest<Request>();
		const response = ctx.getResponse<Response>();
		const { status, headers, ...data } = this.getInfo(exception);
		const result = {
			status,
			...(data && { data }),
		};

		response.status(status);
		if (headers) {
			addHeaders(response, headers);
		}
		response.json(result);

		const logger = request[REQUEST_LOGGER];
		const logMessage = ErrorFilter.getResponseInfo(request, response, exception, data, false);
		const logMessageExt = ErrorFilter.getResponseInfo(request, response, exception, data, true);
		if (logger) {
			// Requests to non-existent handlers and produced by guards are not intercepted with app.interceptor
			logger.error(logMessage);
			logger.trace(logMessageExt);
		} else {
			this.logger.error({
				rid: request.header(RequestHeader.REQUEST_ID),
				...logMessage,
			});
			this.logger.trace({
				rid: request.header(RequestHeader.REQUEST_ID),
				...logMessageExt,
			});
		}

		if (exception instanceof FatalException) {
			throw exception;
		}
	}

	getInfo(error: Error): AppExceptionInfo {
		let info: AppExceptionInfo;

		if (error instanceof HttpException) {
			info = this.getHttpExceptionInfo(error);
		} else if (error instanceof AppException) {
			info = this.getAppExceptionInfo(error);
		} else {
			info = this.getErrorInfo(error);
		}

		if (info.status === HttpStatus.BAD_REQUEST) {
			info.code = info.code || ErrorCode.BAD_REQUEST;
		} else if (info.status === HttpStatus.UNPROCESSABLE_ENTITY) {
			info.code = info.code || ErrorCode.UNPROCESSABLE_ENTITY;
		} else if (info.status === HttpStatus.FORBIDDEN) {
			info.code = info.code || ErrorCode.AUTH_EXCEPTION;
		}

		return info;
	}

	protected getHttpExceptionInfo(exception: HttpException): AppExceptionInfo {
		const status = exception.getStatus();
		const response: string | object = exception.getResponse();
		let message: string | undefined;
		let code: ErrorCode | undefined;
		let data: { [key: string]: string[] } | undefined;

		if (typeof response === 'string') {
			message = response;
		} else if (typeof response === 'object') {
			if (status === HttpStatus.BAD_REQUEST) {
				if (unknownHasProp<{ message: object }>(response, 'message', 'object') && Array.isArray(response.message)) {
					// DTO validation failed
					message = 'Request validation failed';
					code = ErrorCode.BAD_REQUEST_VALIDATION;
					data = {
						errors: response.message.map((item: unknown) => String(item)),
					};
				} else if (unknownHasProp<{ message: string }>(response, 'message', 'string') && response.message.startsWith('Validation failed')) {
					// Param pipeline validation failed
					message = 'Query or path param validation failed';
					code = ErrorCode.BAD_REQUEST_QUERY_OR_PATH_PARAM;
					data = { errors: [response.message] };
				}
				// eslint-disable-next-line max-len
			} else if (unknownHasProp<{ message: string }>(response, 'message', 'string') && unknownHasProp<{ code: string }>(response, 'code', 'string')) {
				message = response.message;
				code = response.code as ErrorCode;
			} else if (unknownHasProp<{ message: string }>(response, 'message', 'string')) {
				message = response.message;
			}
		}

		return {
			status,
			name: exception.constructor?.name || 'HttpException',
			message: message || 'Http exception',
			...(code && { code }),
			...(data && { data }),
		};
	}

	protected getAppExceptionInfo(exception: AppException): AppExceptionInfo {
		const { code, name, message, data } = exception.getInfo();
		let status: number;
		let headers: any | undefined;
		switch (true) {
			case exception instanceof BadParamsException: {
				status = HttpStatus.BAD_REQUEST;
				break;
			}
			case exception instanceof AuthException: {
				status = HttpStatus.FORBIDDEN;
				break;
			}
			case exception instanceof NotExistsException: {
				status = HttpStatus.NOT_FOUND;
				break;
			}
			case exception instanceof UnprocessableException: {
				status = HttpStatus.UNPROCESSABLE_ENTITY;
				break;
			}
			case exception instanceof ServiceUnavailableException: {
				status = HttpStatus.SERVICE_UNAVAILABLE;
				break;
			}
			case exception instanceof NotImplementedException: {
				status = HttpStatus.NOT_IMPLEMENTED;
				break;
			}
			case exception instanceof RecordChangedException: {
				status = HttpStatus.RETRY_WITH;
				break;
			}
			case exception instanceof ConflictException: {
				status = HttpStatus.CONFLICT;
				break;
			}
			default:
				status = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		return {
			...(code && { code }),
			...(headers && { headers }),
			...(data && { data }),
			status,
			name,
			message,
		};
	}

	protected getErrorInfo(error: Error): AppExceptionInfo {
		const status = HttpStatus.INTERNAL_SERVER_ERROR;

		return {
			status,
			name: error instanceof Error ? error?.constructor?.name : 'Error',
			message: error?.message || 'Error',
		};
	}

	/**
	 * Get response info object
	 */
	private static getResponseInfo(request: Request, response: Response, error: Error, data: ExceptionInfo, includeExtraInfo: boolean): LogMessage {
		const requestInfo = getRequestInfo(request, includeExtraInfo);
		const { statusCode: httpStatus } = response;
		const { code: errorCode } = data;

		return {
			type: includeExtraInfo ? LogType.HTTP_ERROR_EXT : LogType.HTTP_ERROR,
			message: `HTTP ${httpStatus}:${errorCode || '-'} (${error.message})`,
			httpStatus,
			...(errorCode && { errorCode }),
			...requestInfo,
			error,
		};
	}
}
