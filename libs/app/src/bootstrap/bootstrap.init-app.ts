import { resolve } from 'path';

import { DynamicModule, ValidationPipe, ValidationPipeOptions, INestApplication, VersioningType } from '@nestjs/common';
import { HttpsOptions } from '@nestjs/common/interfaces/external/https-options.interface';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import * as compression from 'compression';
import * as cookieParser from 'cookie-parser';
import { merge } from 'lodash';

import { BootServiceInterface } from '@binance-test/boot';
import { TypeXOR } from '@binance-test/fixtures';
import { NestJsAdapter } from '@binance-test/logger';

import { REQUEST_LOGGER_NAME } from '../app.constants';
import { ErrorFilter } from '../app.error-filter';
import { AppInterceptor } from '../app.interceptor';
import { AppCommonVarsType } from '../app.vars';

import { SwaggerOptions, initSwagger } from './swagger/swagger.init-swagger';

export type AppOptions = {
	/**
	 * To not setup web-service related features
	 */
	isStandAloneApp?: boolean;
	/**
	 * To enable validation pipe transformation
	 */
	validationPipe?: ValidationPipeOptions;
	/**
	 * Init application swagger JSON scheme available by /swagger url
	 */
	swagger?: SwaggerOptions;
	/**
	 * Disable requests body parser
	 */
	skipBodyParser?: boolean;
	/**
	 * Optional path to expose Prometheus metrics
	 */
	metricPath?: string;

	httpsOptions?: HttpsOptions;
} & TypeXOR<{ appModule: DynamicModule }, { appInstance: INestApplication }>;

/**
 * Init main service app module and start the web service. Could be used for e2e tests too.
 */
export const initApp = async <T extends INestApplication>(bootService: BootServiceInterface, options: AppOptions): Promise<T> => {
	const { appModule, appInstance, validationPipe, swagger, isStandAloneApp, skipBodyParser, metricPath = '/metrics', httpsOptions } = options;

	const loggerService = bootService.getLoggerService();
	const configService = bootService.getConfigService<AppCommonVarsType>();
	const logger = loggerService.getLogger('bootstrap');

	const app =
		appInstance ||
		(await NestFactory.create<NestExpressApplication>(appModule, {
			logger: new NestJsAdapter(logger),
			bodyParser: !skipBodyParser,
			httpsOptions,
		}));

	const appToSet = app as NestExpressApplication;
	appToSet.disable('x-powered-by');
	appToSet.disable('trust proxy');
	appToSet.enable('etag');
	appToSet.set('env', configService.isProd || configService.isUat ? 'production' : 'development');
	appToSet.useStaticAssets(resolve(__dirname, 'assets'));
	appToSet.setBaseViewsDir(resolve(__dirname, 'assets'));
	app.use(compression());
	app.use(cookieParser());
	app.enableShutdownHooks();
	app.useGlobalInterceptors(
		// order matters
		new AppInterceptor(
			loggerService,
			{
				name: REQUEST_LOGGER_NAME,
				skip: [
					{
						type: /^http_ok/,
						url: /^\/health-check/,
					},
				],
			},
			{ metricPath },
		),
	);
	app.useGlobalFilters(new ErrorFilter(loggerService.getLogger(REQUEST_LOGGER_NAME)));

	if (validationPipe) {
		app.useGlobalPipes(
			new ValidationPipe(
				merge(
					{
						validationError: {
							target: false,
							value: true,
						},
					},
					validationPipe,
				),
			),
		);
	}
	app.enableVersioning({
		type: VersioningType.URI,
		prefix: false,
	});

	if (swagger) {
		initSwagger(app, configService, swagger, metricPath);
	}

	if (!isStandAloneApp) {
		const server = app.getHttpServer();

		server.on('listening', () => {
			const { port, address } = server.address();
			logger.info({
				type: 'bootstrap',
				message: `Service is up and running [${address}:${port}]`,
			});
		});
	}

	if (!configService.isTest && !isStandAloneApp) {
		await app.listen(configService.getNumber('APP_PORT', 3000), configService.get('APP_HOST', 'localhost'));
	}

	return app as T;
};
