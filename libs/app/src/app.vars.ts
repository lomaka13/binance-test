/**
 * App config variables
 */

export enum AppCommonVars {
	NODE_ENV,
	APP_NAME,
	APP_VERSION,
	APP_LOG_LEVEL,
	APP_PORT,
	APP_HOST,
}

export type AppCommonVarsType = keyof typeof AppCommonVars;
