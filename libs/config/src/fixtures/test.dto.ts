import { IsNumber, IsObject } from 'class-validator';

export class TestDto {
	@IsNumber()
	test: number;

	@IsObject()
	a: object;
}
