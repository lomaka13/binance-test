export type Handler = (payload: string) => any;

export interface Options {
	[key: string]: string;
}

export interface Handlers {
	[key: string]: Handler;
}

export interface ConfigOptions {
	options?: Options;
	handlers?: Handlers;
}

/**
 * Known env values
 */
export type ConfigEnv = 'local' | 'test' | 'dev' | 'uat' | 'prod';

export const OPTIONAL = 987654103.12;
