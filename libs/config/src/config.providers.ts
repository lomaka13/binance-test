import { Provider } from '@nestjs/common';

import { CONFIG_HANDLERS, CONFIG_OPTIONS } from './config.constants';
import * as defaults from './config.handlers';
import { ConfigOptions } from './config.types';

export const createConfigProviders = ({ options = {}, handlers = {} }: ConfigOptions): Provider[] => {
	return [
		{
			provide: CONFIG_OPTIONS,
			useValue: options,
		},
		{
			provide: CONFIG_HANDLERS,
			useValue: { ...defaults, ...handlers },
		},
	];
};
