import { DeepPartial, DeepRequired, ClassType } from '@binance-test/fixtures';

import { MATCH_IS_IN, MATCH_INSTANCE_OF } from './pipeline.constants';

type Fn = () => any;
type Except = Date | Fn;
export type HandlerResult<T> = DeepPartial<T, Except>;
export type HandlerParam<T> = DeepRequired<T, Except>;

type MatchItem = '*' | any | { [MATCH_IS_IN]: any[] } | { [MATCH_INSTANCE_OF]: ClassType<any> };

export interface PipelinePattern<P, T> {
	pattern?: {
		[key: string]: MatchItem;
	};
	break?: boolean;
	handler: (params: HandlerParam<P>, result: HandlerParam<T>) => Promise<HandlerResult<T>>;
}
