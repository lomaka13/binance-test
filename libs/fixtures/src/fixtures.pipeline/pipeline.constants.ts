export const MATCH_NOT = Symbol();
export const MATCH_INSTANCE_OF = Symbol();
export const MATCH_NOT_INSTANCE_OF = Symbol();
export const MATCH_IS_IN = Symbol();
export const MATCH_NOT_IS_IN = Symbol();
