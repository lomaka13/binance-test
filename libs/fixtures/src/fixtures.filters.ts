/**
 * Filter iterator for typescript to recognize (T | undefined)[] => T[]
 */
export const filterUndefined = <T>(val: T | undefined): val is T => val !== undefined;

/**
 * Filter iterator for typescript to recognize (T | null)[] => T[]
 */
export const filterNull = <T>(val: T | null): val is T => val !== null;

/**
 * Filter iterator for typescript to recognize (T | void)[] => T[]
 */
export const filterVoid = <T>(val: T | void): val is T => val != null;
