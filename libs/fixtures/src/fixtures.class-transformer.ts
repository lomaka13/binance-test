export const stringToBoolean: (value: string) => boolean | null = (value: string): boolean | null => {
	if (value === 'true') {
		return true;
	}
	if (value === 'false') {
		return false;
	}

	return null;
};
