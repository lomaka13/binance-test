import { Inject, Injectable } from '@nestjs/common';

import { FACTORY } from './logger.constants';
import { LoggerHelper } from './logger.helper';
import { Logger, LogMessage, LogLevel, LoggerOptions } from './logger.types';

const logLevels: LogLevel[] = ['trace', 'debug', 'info', 'warn', 'error', 'fatal'];

/**
 * Logger service
 */
@Injectable()
export class LoggerService {
	/**
	 * Creates an instance of LoggerService
	 */
	constructor(private helper: LoggerHelper, @Inject(FACTORY) private loggerFactory: (options?: LoggerOptions) => Logger) {}

	/**
	 * Logger instance factory
	 */
	public getLogger(options?: string | LoggerOptions): Logger {
		return this.wrap(options);
	}

	/**
	 * Create request logger wrapper-instance
	 */
	public wrapRequest(requestId: string, options: string | LoggerOptions): Logger {
		return this.wrap(options, requestId);
	}

	/**
	 * Wrap and return new ready to use logger
	 */
	protected wrap(options: string | LoggerOptions = {}, rid?: string): Logger {
		const instance = this.loggerFactory(this.getOptions(options));
		return logLevels.reduce((logger: any, level: LogLevel): any => {
			logger[level] = (message: any): void => this.logMessage(instance, level, message, rid);
			return logger;
		}, {});
	}

	/**
	 * Get logger options
	 */
	protected getOptions(options: string | LoggerOptions = {}): LoggerOptions {
		if (typeof options === 'string') {
			return {
				name: options,
			};
		}
		return options;
	}

	/**
	 * Perform message logging with given log-level
	 */
	protected logMessage(logger: Logger, level: LogLevel, message: LogMessage, rid?: string): void {
		logger[level](this.transform(message, rid));
	}

	/**
	 * Transform log message
	 */
	protected transform(msg: LogMessage, rid?: string): LogMessage {
		const { type, trace, context, ...message } = msg;
		return {
			rid,
			context,
			trace,
			...this.helper.toObject(message),
			...(type && { type }),
		};
	}
}
