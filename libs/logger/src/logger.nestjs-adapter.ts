import { LoggerService } from '@nestjs/common';

import { Logger, LogLevel, LogType } from './logger.types';

/**
 * Logger adapter for nestjs
 */
export class NestJsAdapter implements LoggerService {
	private logger: Logger;

	constructor(logger: Logger) {
		this.logger = logger;
	}

	public log(message: string): void {
		this.logMessage('info', message);
	}

	public error(message: string, trace: string): void {
		this.logMessage('error', message, trace);
	}

	public warn(message: string): void {
		this.logMessage('warn', message);
	}

	public debug(message: string): void {
		this.logMessage('debug', message);
	}

	public verbose(message: string): void {
		this.logMessage('trace', message);
	}

	protected logMessage(level: LogLevel, message: string, trace?: string): void {
		this.logger[level]({
			type: LogType.NEST_MESSAGE,
			message,
			trace,
		});
	}
}
