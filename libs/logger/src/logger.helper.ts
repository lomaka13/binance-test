import { inspect as utilInspect, InspectOptions } from 'util';

import { Injectable, Inject } from '@nestjs/common';

import { LOGGER_CONFIG } from './logger.constants';
import { LogMessage, LoggerConfigOptions, LogType } from './logger.types';

/**
 * Logger helper class
 */
@Injectable()
export class LoggerHelper {
	private inspectOptions: InspectOptions;

	/**
	 * Creates helper instance
	 */
	constructor(@Inject(LOGGER_CONFIG) private config: LoggerConfigOptions) {
		this.inspectOptions = {
			breakLength: Infinity,
			compact: true,
		};
		if (config.inspectDepth) {
			this.inspectOptions.depth = config.inspectDepth;
		}
		if (config.inspectMaxArrayLenght) {
			this.inspectOptions.maxArrayLength = config.inspectMaxArrayLenght;
		}
	}

	/**
	 * Normalize log message
	 */
	public toObject(data: any): LogMessage {
		let logData = data;
		if (Array.isArray(data) && data.length === 1) {
			logData = data[0];
		}
		if (Array.isArray(logData)) {
			return {
				type: LogType.MALFORMED_LOG_MESSAGE,
				message: this.stringify(data),
			};
		}
		if (typeof logData === 'string' || logData instanceof String) {
			logData = {
				message: this.cutString(logData.toString()),
			};
		}
		if (!('type' in logData)) {
			logData.type = LogType.LOG_MESSAGE;
		}
		return this.safeLogData(logData);
	}

	private safeLogData(data: any): any {
		const safe = (this.config.nonSafeDataFields || []).filter((name: string) => name in data);
		let maxLen: number;
		if (safe.length && this.config.maxStringLength) {
			maxLen = Math.floor(this.config.maxStringLength / safe.length);
		}
		safe.forEach((field: string) => {
			const value = data[field];
			if (value) {
				delete data[field];
				data[`${field}:safe`] = this.cutString(this.stringify(value), maxLen);
			}
		});
		return data;
	}

	private stringify(data: any): string {
		return utilInspect(data, this.inspectOptions);
	}

	private cutString(str: string, maxLen?: number): string {
		const len = maxLen || this.config.maxStringLength;
		if (!len) {
			return str;
		}
		return String(str).substring(0, len - 3) + '...';
	}
}
