# Test utils

Different commong mocks and tools for testing

### HttpServer mock

```ts
import { getServerMock, HttpServerMockApi } from '@wlx/common/test-utils';

let server: http.Server;
let serverMock: HttpServerMockApi;
beforeAll(async () => {
	serverMock = getServerMock();
	server = await serverMock.startServer();
});

afterAll(async () => {
	await serverMock.closeServer();
});

afterEach(() => {
	serverMock.resetMocks();
});

it('should make request and get NO CONTENT response', async () => {
	serverMock.mocks.responseStatus.mockReturnValue(204);
	const response = await request();
	expect(response.status).toEqual(204);
});

it('should make request and get OK response with headers', async () => {
	const list = {
		content: Date.now(),
	};
	const headers = {
		'x-app-header': String(Date.now()),
	};
	serverMock.mocks.responseText.mockReturnValue(JSON.stringify(list));
	serverMock.mocks.responseHeaders.mockReturnValue(headers);
	const response = await request();
	expect(response.status).toEqual(204);
	expect(res).toMatchObject({
		status: 200,
		headers,
		list,
	});
});

it('should make request and get TIMEOUT error', async () => {
	serverMock.mocks.responseTimeout.mockReturnValue(1000);
	expect(request({ timeout: 100 })).rejects.toThrow('timeout of 100ms exceeded');
});

```

### Jest extended expect matchers
Available matchers:
- toBeIntegerString
- toBePositiveIntegerString
- toBeDecimalString
- toBePositiveDecimalString
- toBeUUID

```ts
import { toBeDecimalString } from '@wlx/common/test-utils';

expect.extend({
  toBeDecimalString
});

it('should be decimal string', () => {
  expect('42.24').toBeDecimalString();
});

it('should not be decimal string', () => {
  expect('abc').not.toBeDecimalString();
});
```
