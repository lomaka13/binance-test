import { INestApplication } from '@nestjs/common';
import { ModuleMetadata } from '@nestjs/common/interfaces';
import { Test } from '@nestjs/testing';

import { initBoot, initApp as initAppOriginal, AppOptions } from '@binance-test/app';
import { BootServiceInterface } from '@binance-test/boot';
import { ConfigGlobalModule } from '@binance-test/config';
import { LoggerGlobalModule } from '@binance-test/logger';

import { Override, applyOverrides } from './overrides';

export { initBoot };

type TestAppOptions = Omit<AppOptions, 'appIstance' | 'appModule'> & {
	appInstance?: AppOptions['appInstance'];
	testModule?: ModuleMetadata;
	overrides?: Override[];
};

export const initApp = async <T extends INestApplication>(bootService: BootServiceInterface, options: TestAppOptions): Promise<T> => {
	const { appInstance, isStandAloneApp, testModule, overrides } = options;

	if (!isStandAloneApp && !appInstance && testModule) {
		(testModule.imports = testModule.imports || []).unshift(ConfigGlobalModule.forRoot(bootService), LoggerGlobalModule.forRoot(bootService));
		const builder = Test.createTestingModule(testModule);
		applyOverrides(builder, overrides);
		const module = await builder.compile();
		options.appInstance = module.createNestApplication();
	}

	return initAppOriginal(bootService, {
		validationPipe: {},
		swagger: {},
		...(options as any),
	});
};
