import { toBeDateString } from './matchers/toBeDateString';
import { toBeDecimalString } from './matchers/toBeDecimalString';
import { toBeIntegerString } from './matchers/toBeIntegerString';
import { toBePositiveDecimalString } from './matchers/toBePositiveDecimalString';
import { toBePositiveIntegerString } from './matchers/toBePositiveIntegerString';
import { toBeUUID } from './matchers/toBeUUID';

declare global {
	// eslint-disable-next-line @typescript-eslint/no-namespace
	namespace jest {
		interface Matchers<R> {
			toBeDateString(): R;
			toBeIntegerString(): R;
			toBeIntegerString(): R;
			toBePositiveIntegerString(): R;
			toBeDecimalString(): R;
			toBePositiveDecimalString(): R;
			toBeUUID(): R;
		}
		interface Expect {
			toBeDateString(): void;
			toBeIntegerString(): void;
			toBePositiveIntegerString(): void;
			toBeDecimalString(): void;
			toBePositiveDecimalString(): void;
			toBeUUID(): void;
		}
	}
}

expect.extend({
	toBeDateString,
	toBeIntegerString,
	toBePositiveIntegerString,
	toBeDecimalString,
	toBePositiveDecimalString,
	toBeUUID,
});

export { toBeDateString, toBeIntegerString, toBePositiveIntegerString, toBeDecimalString, toBePositiveDecimalString, toBeUUID };
