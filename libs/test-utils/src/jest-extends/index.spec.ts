import './index';

describe('Common -> Test Utils -> Jest Extend Matchers', () => {
	it('toBeIntegerString', () => {
		expect('42').toBeIntegerString();
	});

	it('not.toBeIntegerString', () => {
		expect(23).not.toBeIntegerString();
	});

	it('toBePositiveIntegerString', () => {
		expect('42').toBePositiveIntegerString();
	});

	it('not.toBePositiveIntegerString', () => {
		expect('-42').not.toBePositiveIntegerString();
	});

	it('toBeDecimalString', () => {
		expect('42.24').toBeDecimalString();
	});

	it('not.toBeDecimalString', () => {
		expect(' 23').not.toBeDecimalString();
	});

	it('toBePositiveDecimalString', () => {
		expect('42.24').toBePositiveDecimalString();
	});

	it('not.toBePositiveDecimalString', () => {
		expect('-42.8').not.toBePositiveDecimalString();
	});

	it('toBeUUID', () => {
		expect('e3a1f889-e6b5-4f59-97f8-e8fe2992aef7').toBeUUID();
	});

	it('not.toBeUUID', () => {
		expect('e3a1f889-e6b5-4f59-97f8').not.toBeUUID();
	});
});
