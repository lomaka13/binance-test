export const isInt = (value: any): boolean => {
	const n = Math.floor(Number(value));
	return isFinite(n) && String(n) === value;
};

export const isNumeric = (value: any): boolean => {
	if (typeof value !== 'string') {
		return false;
	}

	return value.length > 0 && !value.includes(' ') && !isNaN(value as any);
};
