import { isDateString } from 'class-validator';
import { matcherHint, printReceived } from 'jest-matcher-utils';

const passMessage =
	(received: any): (() => string) =>
	(): string =>
		matcherHint('.not.toBeDateString', 'received', '') +
		'\n\n' +
		'Expected value to not be date string, received:\n' +
		`  ${printReceived(received)}`;

const failMessage =
	(received: any): (() => string) =>
	(): string =>
		matcherHint('.toBeDateString', 'received', '') + '\n\n' + 'Expected value to be date string, received:\n' + `  ${printReceived(received)}`;

export const toBeDateString = (received: any): jest.CustomMatcherResult => {
	const pass = isDateString(received);

	if (pass) {
		return { pass: true, message: passMessage(received) };
	}

	return { pass: false, message: failMessage(received) };
};
