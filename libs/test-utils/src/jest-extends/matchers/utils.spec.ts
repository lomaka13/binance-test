import { isInt, isNumeric } from './utils';

describe('Common -> Libs -> Test Utils -> Jest Extends', () => {
	describe('Test Integer String ', () => {
		const testCases = [
			{ value: '42', expected: true },
			{ value: '-42', expected: true },
			{ value: '0', expected: true },
			{ value: '4e2', expected: false },
			{ value: '42.9', expected: false },
			{ value: 42, expected: false },
			{ value: 4e2, expected: false },
			{ value: 42.1, expected: false },
			{ value: ' 1', expected: false },
			{ value: '1 ', expected: false },
			{ value: ' 1 ', expected: false },
			{ value: '', expected: false },
			{ value: ' ', expected: false },
			{ value: '1a', expected: false },
			{ value: [], expected: false },
			{ value: null, expected: false },
			{ value: undefined, expected: false },
			{ value: NaN, expected: false },
			{ value: Infinity, expected: false },
			{ value: -Infinity, expected: false },
		];
		for (const testCase of testCases) {
			const { value, expected } = testCase;
			it(`Check value ${value}`, () => {
				expect(isInt(value)).toEqual(expected);
			});
		}
	});

	describe('Test Numeric String ', () => {
		const testCases = [
			{ value: '42', expected: true },
			{ value: '-42', expected: true },
			{ value: '42.9', expected: true },
			{ value: '.9', expected: true },
			{ value: '.9', expected: true },
			{ value: '0', expected: true },
			{ value: 42, expected: false },
			{ value: 4e2, expected: false },
			{ value: 42.1, expected: false },
			{ value: ' 1', expected: false },
			{ value: '1 ', expected: false },
			{ value: ' 1 ', expected: false },
			{ value: '', expected: false },
			{ value: ' ', expected: false },
			{ value: '1a', expected: false },
			{ value: [], expected: false },
			{ value: null, expected: false },
			{ value: undefined, expected: false },
			{ value: NaN, expected: false },
			{ value: Infinity, expected: false },
			{ value: -Infinity, expected: false },
		];
		for (const testCase of testCases) {
			const { value, expected } = testCase;
			it(`Check value ${value}`, () => {
				expect(isNumeric(value)).toEqual(expected);
			});
		}
	});
});
