import { plainToClass } from 'class-transformer';
import { validate, validateOrReject, ValidationError } from 'class-validator';
import * as _ from 'lodash';

import { filterUndefined, ClassType } from '@binance-test/fixtures';

export async function transformAndValidate<T extends object>(classType: ClassType<T>, something: object | string): Promise<T> {
	return new Promise((resolve: (...args: any[]) => void, reject: (...args: any[]) => void): void => {
		let plainObject: object;
		if (typeof something === 'string') {
			plainObject = JSON.parse(something);
		} else if (something != null && typeof something === 'object') {
			plainObject = something;
		} else {
			return reject(new Error('Incorrect object param type!'));
		}

		const classObject = plainToClass(classType, plainObject);

		if (Array.isArray(classObject)) {
			Promise.all(classObject.map((objectElement: any) => validate(objectElement))).then((errors: any) =>
				errors.every((error: ValidationError[]) => error.length === 0) ? resolve(classObject) : reject(errors),
			);
		} else {
			validateOrReject(classObject)
				.then(() => resolve(classObject))
				.catch(reject);
		}
	});
}

export function isValidationError(e: any): e is ValidationError {
	return e instanceof ValidationError || (e instanceof Array && e.every((i: any) => i instanceof ValidationError));
}

const prependConstraintsWithParentProp = (parentError: ValidationError, currentError: ValidationError): ValidationError => {
	const error: ValidationError = {
		...currentError,
	};
	const constraints: { [key: string]: string } = (error.constraints = {
		...error.constraints,
	});
	Object.keys(constraints).forEach((key: string) => {
		constraints[key] = `${parentError.property}.${constraints[key]}`;
	});
	return error;
};

const mapChildrenToValidationErrors = (error: ValidationError): ValidationError[] => {
	if (!(error.children && error.children.length)) {
		return [error];
	}
	const errors: ValidationError[] = [];
	for (const item of error.children) {
		if (item.children && item.children.length) {
			errors.push(...mapChildrenToValidationErrors(item));
		}
		errors.push(prependConstraintsWithParentProp(error, item));
	}
	return errors;
};

export const flattenValidationErrors = (validationErrors: ValidationError[]): string[] => {
	return _(validationErrors)
		.map(mapChildrenToValidationErrors)
		.flatten()
		.map((error: ValidationError) => {
			if (error.constraints) {
				return Object.values(error.constraints);
			}
			return undefined;
		})
		.filter(filterUndefined)
		.flatten()
		.value();
};
