import { ConfigService } from '@binance-test/config';
import { NotExistsException, ErrorCode } from '@binance-test/exceptions';
import { createConfigServiceMock } from '@binance-test/mocks/config';
import { createExecutionContext } from '@binance-test/test-utils';

import { DevOnlyGuard } from './dev-only.guard';

describe('Test-utils > Decorators > DevOnlyGuard', () => {
	let contextExecutor: ReturnType<typeof createExecutionContext>;
	let guard: DevOnlyGuard;

	it('should be defined', () => {
		expect(new DevOnlyGuard(createConfigServiceMock<ConfigService>())).toBeDefined();
	});

	it('should return true for test env', async () => {
		const configMock = createConfigServiceMock<ConfigService>(
			{},
			{
				env: 'test',
				isTest: true,
			},
		);
		guard = new DevOnlyGuard(configMock);
		const result = await guard.canActivate(contextExecutor);
		expect(result).toBe(true);
	});

	it('should return true for local env', async () => {
		const configMock = createConfigServiceMock<ConfigService>(
			{},
			{
				env: 'local',
				isLocal: true,
				isDev: true,
				isTest: false,
			},
		);
		guard = new DevOnlyGuard(configMock);
		const result = await guard.canActivate(contextExecutor);
		expect(result).toBe(true);
	});

	it('should return true for dev env', async () => {
		const configMock = createConfigServiceMock<ConfigService>(
			{},
			{
				env: 'dev',
				isDev: true,
				isTest: false,
			},
		);
		guard = new DevOnlyGuard(configMock);
		const result = await guard.canActivate(contextExecutor);
		expect(result).toBe(true);
	});

	it('should throw 404 error for uat env', async () => {
		const configMock = createConfigServiceMock<ConfigService>(
			{},
			{
				env: 'uat',
				isUat: true,
				isTest: false,
			},
		);
		guard = new DevOnlyGuard(configMock);
		try {
			await guard.canActivate(contextExecutor);
		} catch (error: any) {
			expect(error instanceof NotExistsException).toBe(true);
			expect(error.code).toEqual(ErrorCode.NOT_EXISTS);
		}
	});

	it('should throw 404 error for prod env', async () => {
		const configMock = createConfigServiceMock<ConfigService>(
			{},
			{
				env: 'prod',
				isProd: true,
				isTest: false,
			},
		);
		guard = new DevOnlyGuard(configMock);
		try {
			await guard.canActivate(contextExecutor);
		} catch (error: any) {
			expect(error instanceof NotExistsException).toBe(true);
			expect(error.code).toEqual(ErrorCode.NOT_EXISTS);
		}
	});
});
