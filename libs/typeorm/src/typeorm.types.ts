export * from 'typeorm';
export * from 'typeorm/error/EntityNotFoundError';

export interface TypeOrmConfigProviderOptions {
	migrationsDir?: string;
	entities?: any[];
	provide?: string;
	useFactory?: (...params: any[]) => void;
	inject?: any[];
}

export interface OptionalFieldsInterface {
	id?: any;
	createdAt?: any;
	updatedAt?: any;
}
type OptionalFieldsBeforeSave = keyof OptionalFieldsInterface;
export type EntityBeforeInsert<T extends OptionalFieldsInterface> = Pick<Partial<T>, OptionalFieldsBeforeSave> & Omit<T, OptionalFieldsBeforeSave>;
