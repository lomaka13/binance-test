import { Logger as TypeOrmLogger, QueryRunner } from 'typeorm';

import { Logger, LogLevel, LogMessage } from '@binance-test/logger';

export type LogCategory = 'query' | 'schema' | 'error' | 'warn' | 'info' | 'log' | 'migration';
export type TypeormLoggerOptions = 'all' | LogCategory[];

export class LoggerAdapter implements TypeOrmLogger {
	constructor(private readonly logger: Logger, private categories: TypeormLoggerOptions) {}

	/**
	 * Logs query and parameters used in it
	 */
	logQuery(query: string, params?: any[], queryRunner?: QueryRunner): void {
		this.performLog('info', 'query', {
			type: 'typeorm_query',
			message: `TypeOrm query`,
			query,
			params,
		});
	}

	/**
	 * Logs query that is failed
	 */
	logQueryError(error: string, query: string, params?: any[], queryRunner?: QueryRunner): void {
		this.performLog('error', 'error', {
			type: 'typeorm_query_error',
			message: `TypeOrm query error`,
			error,
			query,
			params,
		});
	}

	/**
	 * Logs query that is slow
	 */
	logQuerySlow(time: number, query: string, params?: any[], queryRunner?: QueryRunner): void {
		this.logger.warn({
			type: 'typeorm_slow_query',
			message: `TypeOrm slow query`,
			slowQueryTime: time,
			query,
			params,
		});
	}

	/**
	 * Logs events from the schema build process
	 */
	logSchemaBuild(message: string, queryRunner?: QueryRunner): void {
		this.performLog('info', 'schema', {
			type: 'typeorm_schema',
			message,
		});
	}

	/**
	 * Logs events from the migrations run process
	 */
	logMigration(message: string, queryRunner?: QueryRunner): void {
		this.performLog('info', 'migration', {
			type: 'typeorm_migration',
			message,
		});
	}

	/**
	 * Logging by level
	 */
	log(category: 'log' | 'info' | 'warn', message: any, queryRunner?: QueryRunner): void {
		this.performLog(category === 'warn' ? 'warn' : 'info', category, {
			type: 'typeorm_message',
			message: `TypeOrm log message`,
			data: message,
		});
	}

	private performLog(level: LogLevel, category: LogCategory, message: LogMessage): void {
		if (this.categories === 'all' || this.categories.includes(category)) {
			// tslint:disable-next-line:tsr-detect-unsafe-properties-access
			this.logger[level](message);
		}
	}
}
