import { ConfigService } from '@binance-test/config';
import { LoggerService } from '@binance-test/logger';

export interface BootServiceInterface {
	/**
	 * Get ready to use config service
	 */
	getConfigService<T extends string>(): ConfigService<T>;

	/**
	 * Get ready to use logger service
	 */
	getLoggerService(): LoggerService;

	/**
	 * Enable handlers for exceptional process messages
	 */
	enableProcessExceptionsHooks(): void;
}
