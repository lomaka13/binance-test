import { BootServiceInterface } from '@binance-test/boot';
import { createConfigServiceMock } from '@binance-test/mocks/config';
import { createLoggerServiceMock } from '@binance-test/mocks/logger';

import { BootServiceMock } from './boot.service.mock';

export const createBootServiceMock = ({
	configService = createConfigServiceMock(),
	loggerService = createLoggerServiceMock(),
}: {
	configService?: any;
	loggerService?: any;
} = {}): BootServiceInterface => new BootServiceMock(configService, loggerService);
