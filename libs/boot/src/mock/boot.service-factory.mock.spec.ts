import { createBootServiceMock } from './boot.service-factory.mock';
import { BootServiceMock } from './boot.service.mock';

describe('Common > boot > Mock > createBootServiceMock', () => {
	afterEach(() => {
		jest.resetAllMocks();
	});

	it('should create boot service mock', () => {
		const bootService = createBootServiceMock();
		expect(bootService).toBeDefined();
		expect(bootService).toBeInstanceOf(BootServiceMock);
	});

	it('should provide default configService mock', () => {
		const configService: any = createBootServiceMock().getConfigService();
		expect(configService).toBeDefined();
		expect(configService.get).toBeCalledTimes(0);
		expect(configService.set).toBeCalledTimes(0);
		expect(configService.has).toBeCalledTimes(0);
	});

	it('should provide default loggerService mock', () => {
		const loggerService: any = createBootServiceMock().getLoggerService();
		expect(loggerService).toBeDefined();
		expect(loggerService.getLogger).toBeCalledTimes(0);
		expect(loggerService.wrapRequest).toBeCalledTimes(0);
	});
});
