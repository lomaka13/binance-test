# Redis client module

Redis client connection module

### Usage example
Init module with default config factory.

```
import { Module } from '@nestjs/common';

import { RedisModule } from '@binance-test/redis';

@Module({
	imports: [
		RedisModule.register(),
	],
})
export class RedisClientModule {
}

```

Usage as service

```
import { Injectable } from '@nestjs/common';
import { RedisService } from '@binance-test/redis';


export class ExampleService {
	constructor(
		protected redisService: RedisService,
	) {
	}

	getInfo() {
		return this.redisService.getClient().info();
	}
}

```

### Additional links
[nestjs-redis](https://github.com/kyknow/nestjs-redis#readme) - ioredis adapter for nestjs 

[ioredis](https://github.com/luin/ioredis)
