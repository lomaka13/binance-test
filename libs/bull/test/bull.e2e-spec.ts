import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { Promise } from 'bluebird';

import { initBoot } from '@binance-test/app';
import { RedisVars } from '@binance-test/redis';

import { AppModule, QueueProcessorService, QueueService } from './bull-test.module';

import './test.env';

describe.skip('Libs > Bull (e2e)', () => {
	let app: INestApplication;
	let sQueue: QueueService;
	let sQueueProc: QueueProcessorService;

	beforeAll(async () => {
		const bootService = await initBoot({
			configVars: RedisVars,
		});
		const moduleFixture: TestingModule = await Test.createTestingModule({
			imports: [
				AppModule.forRoot(bootService),
			],
		})
		.compile();

		app = moduleFixture.createNestApplication();

		await app.init();

		sQueue = moduleFixture.get(QueueService);
		sQueueProc = moduleFixture.get(QueueProcessorService);
	});

	beforeEach(async () => {
		jest.clearAllTimers();
		jest.clearAllMocks();
	});

	afterAll(async () => {
		await app.close();
	});

	it('check flow', async () => {
		const runSpy = jest.spyOn(sQueueProc, 'checkRun');
		const job = await sQueue.addJob({ hello: 'world' });
		await Promise.delay(100);
		expect(runSpy).toHaveBeenCalledWith(job.id);
	});

});
