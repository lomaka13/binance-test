process.env.APP_REDIS_HOST = process.env.APP_REDIS_HOST || 'localhost';
process.env.APP_REDIS_PORT = process.env.APP_REDIS_PORT || '6379';
process.env.APP_REDIS_PASSWORD = process.env.APP_REDIS_PASSWORD || 'qwerty123';
process.env.APP_LOG_LEVEL = process.env.APP_LOG_LEVEL || 'fatal';
