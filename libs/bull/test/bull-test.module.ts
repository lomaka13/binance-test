import { DynamicModule, Module, Injectable } from '@nestjs/common';

import { BootServiceInterface } from '@binance-test/boot';
import { LoggerGlobalModule, InjectLogger, Logger } from '@binance-test/logger';
import { ConfigGlobalModule } from '@binance-test/config';

import {
	BullModule,
	BullQueueService,
	InjectQueue,
	Queue,
	BullLoggerService,
	Process,
	Job,
	Processor,
} from '../src';

@Injectable()
export class QueueService extends BullQueueService {
	constructor(
		@InjectQueue('test') readonly queue: Queue,
	) {
		super();
	}
}

@Processor('test')
export class QueueProcessorService extends BullLoggerService {
	constructor(
		@InjectLogger('test-queue') readonly logger: Logger,
	) {
		super();
	}

	@Process()
	async runJob(job: Job<any>): Promise<void> {
		this.checkRun(job.id);
	}

	checkRun(jobId: any): void {
		return jobId;
	}
}

@Module({})
export class AppModule {
	public static forRoot(bootService: BootServiceInterface): DynamicModule {
		return {
			module: AppModule,
			providers: [
				QueueService,
				QueueProcessorService,
			],
			imports: [
				ConfigGlobalModule.forRoot(bootService),
				LoggerGlobalModule.forRoot(bootService),
				BullModule.registerQueue({
					name: 'test',
				}),
			],
		};
	}
}
