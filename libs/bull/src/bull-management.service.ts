import { Injectable, OnModuleDestroy } from '@nestjs/common';
import * as BullQueue from 'bull';

import { AppException } from '@binance-test/exceptions';

import { BullManyOptions } from '.';

@Injectable()
export class BullManagementService implements OnModuleDestroy {
	private readonly queues: Map<string, BullQueue.Queue> = new Map();

	constructor(options: BullManyOptions[]) {
		for (const { name, queueOptions } of options) {
			this.queues.set(name, new BullQueue(name, queueOptions));
		}
	}

	list(): Map<string, BullQueue.Queue> {
		return this.queues;
	}

	getOne(name: string): BullQueue.Queue | null {
		return this.queues.get(name) || null;
	}

	getOneOrFail(name: string): BullQueue.Queue {
		if (!this.queues.has(name)) {
			throw new AppException(`Queue with name ${name} not registered`);
		}
		return this.queues.get(name)!;
	}

	async onModuleDestroy(): Promise<void> {
		await Promise.all(Array.from(this.queues).map(([name, queue]: [string, BullQueue.Queue]): Promise<void> => queue.close()));
	}
}
