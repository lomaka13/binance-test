import { Injectable } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as faker from 'faker';

import { ConfigModuleMock } from '@binance-test/mocks/config';

import { InjectQueue, Queue, BullModule } from '.';
import { BullQueueService } from './bull-queue.service';

jest.mock('bull');

@Injectable()
class QueueService extends BullQueueService {
	constructor(@InjectQueue('email') readonly queue: Queue) {
		super();
	}
}

describe.skip('BullQueueService', () => {
	let service: QueueService;
	const mockData = { status: 1 };
	const fakeProcessor = jest.fn();

	beforeAll(async () => {
		const module: TestingModule = await Test.createTestingModule({
			imports: [
				ConfigModuleMock.register(),
				BullModule.registerQueue({
					name: 'email',
					useFactory: (): any => ({
						processors: [fakeProcessor],
						redis: {
							host: '0.0.0.0',
							port: 6380,
						},
					}),
				}),
			],
			providers: [QueueService],
		}).compile();

		service = module.get<QueueService>(QueueService);
		(service as any).queue.add = jest.fn().mockReturnValue(mockData);
		(service as any).queue.getJob = jest.fn().mockReturnValue(mockData);
	});

	it('should be defined', () => {
		expect(service).toBeDefined();
	});

	it('should contains addJob method', () => {
		expect(service.addJob).toBeDefined();
	});

	it('should contains getJob method', () => {
		expect(service.getJob).toBeDefined();
	});

	it('should add to queue with mockData', async () => {
		const msg = {
			to: faker.internet.email(),
			templateId: 'test_id',
			dynamicTemplateData: {
				someOption: 'test_option',
			},
		};
		const result = await service.addJob(msg);
		expect(result).toStrictEqual(mockData);
	});

	it('should get queue with mockData', async () => {
		const result = await service.getJob(String(faker.random.number(100)));
		expect(result).toStrictEqual(mockData);
	});
});
