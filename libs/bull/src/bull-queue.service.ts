import { Queue, Job, JobOptions } from 'bull';

export abstract class BullQueueService {
	protected abstract queue: Queue;

	async addJob(data: any, options?: JobOptions): Promise<Job> {
		return this.queue.add(data, {
			removeOnComplete: true,
			...options,
		});
	}

	async getJob(jobId: string): Promise<Job | null> {
		return this.queue.getJob(jobId);
	}

	async pauseQueue(): Promise<void> {
		return this.queue.pause();
	}

	async resumeQueue(): Promise<void> {
		return this.queue.resume();
	}
}
