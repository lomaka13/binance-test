import { BullModule as BullNestModule, BullModuleOptions } from '@nestjs/bull';
import { Module, DynamicModule } from '@nestjs/common';
import { QueueOptions } from 'bull';
import { castArray } from 'lodash';

import { ConfigService } from '@binance-test/config';
import { RedisVarsType, createConnectionOptions } from '@binance-test/redis';

import { BullManagementService } from './bull-management.service';

export interface BullConfigOptions {
	name: string;
	useFactory?: (...args: any[]) => Promise<BullModuleOptions> | BullModuleOptions;
}

export interface BullManyOptions {
	name: string;
	queueOptions: QueueOptions;
}

@Module({})
export class BullModule {
	static registerQueue(options: BullConfigOptions | BullConfigOptions[]): DynamicModule {
		return {
			imports: [
				BullNestModule.registerQueueAsync(
					...castArray(options).map((option: BullConfigOptions) => ({
						name: option.name,
						useFactory: option.useFactory || this.configFactory,
						...(!option.useFactory && { inject: [ConfigService] }),
					})),
				),
			],
			module: BullModule,
			exports: [BullModule, BullNestModule],
		};
	}

	static registerManagementQueues(options: BullManyOptions[]): DynamicModule {
		return {
			providers: [
				{
					provide: BullManagementService,
					useFactory: (): BullManagementService => {
						return new BullManagementService(options);
					},
				},
			],
			module: BullModule,
			exports: [BullModule, BullManagementService],
		};
	}

	static configFactory(config: ConfigService<RedisVarsType>): BullModuleOptions {
		return {
			redis: createConnectionOptions(config),
		};
	}
}
