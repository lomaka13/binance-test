import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, Index } from '@binance-test/typeorm';

import { SymbolsEntity } from './symbols.entity';

@Entity('tickers')
@Index(['price', 'timestamp'])
export class TickersEntity {
	@PrimaryGeneratedColumn('uuid')
	id: string;

	@Column('varchar')
	price: string;

	@Column('timestamp')
	timestamp: Date;

	@ManyToOne(() => SymbolsEntity, ({ symbol }: SymbolsEntity) => symbol, {
		onDelete: 'CASCADE',
	})
	@JoinColumn({ name: 'symbolId' })
	symbol: SymbolsEntity;

	@CreateDateColumn()
	createdAt: Date;

	@UpdateDateColumn()
	updatedAt: Date;
}
