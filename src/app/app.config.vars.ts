import { AppCommonVars, AppCommonVarsType } from '@binance-test/app';
import { RedisVars, RedisVarsType } from '@binance-test/redis';
import { TypeOrmVars, TypeOrmVarsType } from '@binance-test/typeorm';

enum Vars {}

export const AppVars = { ...AppCommonVars, ...RedisVars, ...TypeOrmVars, ...Vars };

export type AppVarsType = AppCommonVarsType | RedisVarsType | TypeOrmVarsType | keyof typeof Vars;
