import { DynamicModule, Module } from '@nestjs/common';

import { AppController, AppService } from '@binance-test/app';
import { BootServiceInterface } from '@binance-test/boot';
import { ConfigGlobalModule } from '@binance-test/config';
import { LoggerGlobalModule } from '@binance-test/logger';
import { RedisModule } from '@binance-test/redis';

import { BinanceModule } from '../binance/binance.module';
import { EntitiesModule } from '../entities/entities.module';
import { SymbolsModule } from '../symbols/symbols.module';

@Module({
	controllers: [AppController],
	providers: [AppService],
})
export class AppModule {
	public static forRoot(bootService: BootServiceInterface): DynamicModule {
		return {
			module: AppModule,
			imports: [
				ConfigGlobalModule.forRoot(bootService),
				LoggerGlobalModule.forRoot(bootService),
				RedisModule.registerMulti([{}, { name: 'subscriber' }, { name: 'publisher' }]),
				EntitiesModule,
				SymbolsModule,
				BinanceModule,
			],
		};
	}
}
