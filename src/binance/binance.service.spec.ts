import { Test, TestingModule } from '@nestjs/testing';

import { ConfigGlobalModuleMock } from '@binance-test/mocks/config';
import { LoggerGlobalModuleMock } from '@binance-test/mocks/logger';
import { RedisService } from '@binance-test/redis';

import { SymbolsService } from '../symbols/symbols.service';
import { TickersService } from '../tickers/tickers.service';

import { BinanceService } from './binance.service';

describe('BinanceService', () => {
	let service: BinanceService;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			imports: [ConfigGlobalModuleMock.register(), LoggerGlobalModuleMock.register()],
			providers: [
				BinanceService,
				{ provide: SymbolsService, useValue: jest.fn() },
				{ provide: TickersService, useValue: jest.fn() },
				{ provide: RedisService, useValue: jest.fn() },
			],
		}).compile();

		service = module.get<BinanceService>(BinanceService);
	});

	it('should be defined', () => {
		expect(service).toBeDefined();
	});
});
