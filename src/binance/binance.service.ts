import { Injectable, Inject, OnModuleInit } from '@nestjs/common';
import * as WebSocket from 'ws';

import { InjectLogger, Logger } from '@binance-test/logger';
import { RedisService } from '@binance-test/redis';

import { SYMBOLS_CHANNEL } from '../app/app.constants';
import { SymbolsService } from '../symbols/symbols.service';
import { TickersService } from '../tickers/tickers.service';

import { BinanceMessageInterface } from './binance.message.interface';

@Injectable()
export class BinanceService implements OnModuleInit {
	private ws: WebSocket;

	private nonce: number = 0;

	private readonly symbolsSet: Set<string> = new Set<string>();

	constructor(
		@InjectLogger('binance-service') private readonly logger: Logger,
		@Inject(SymbolsService) private readonly symbols: SymbolsService,
		@Inject(TickersService) private readonly tickers: TickersService,
		@Inject(RedisService) private readonly redis: RedisService,
	) {}

	async onModuleInit(): Promise<void> {
		const symbols = await this.symbols.list();
		for (const { symbol } of symbols) {
			this.symbolsSet.add(symbol);
		}
		this.ws = new WebSocket('wss://stream.binance.com:9443/ws');
		this.ws.on('message', (data: string) => {
			const parsedData: BinanceMessageInterface & { result: unknown } = JSON.parse(data);
			if (parsedData.result === null) {
				return;
			}
			this.saveTicker(parsedData);
		});

		this.ws.on('open', () => {
			if (this.symbolsSet.size > 0) {
				const payload = {
					method: 'SUBSCRIBE',
					params: [...this.symbolsSet].map((symbol: string) => `${symbol}@ticker`),
					id: this.nonce++,
				};
				this.ws.send(JSON.stringify(payload));
			}
		});
		this.subscribeOnSymbolsChange();
	}

	private subscribeOnSymbolsChange(): void {
		const redisClient = this.redis.getClient('subscriber');
		redisClient.subscribe(SYMBOLS_CHANNEL);

		redisClient.on('message', (channel: string, message: string) => {
			const [symbol, action] = message.split(':');
			this.updateSubscription(symbol, action);
		});
	}

	private async saveTicker(ticker: BinanceMessageInterface): Promise<void> {
		const lockKey = `lockers:${ticker.s}:${ticker.E}`;
		const redisClient = this.redis.getClient();
		await this.redis.concurrencyWrite(redisClient, lockKey, async (locked: boolean) => {
			if (locked) {
				this.logger.debug({
					type: 'DEBUG',
					message: `${lockKey} - locked`,
				});
				return;
			}
			await redisClient.set(`prices:${ticker.s}`, ticker.c);
			await this.tickers.addPrice(ticker.s, ticker.c, new Date(ticker.E));
		});
	}

	private updateSubscription(symbol: string, action: string): void {
		switch (action) {
			case 'created': {
				this.symbolsSet.add(symbol);
				const payload = {
					method: 'SUBSCRIBE',
					params: [`${symbol}@ticker`],
					id: this.nonce++,
				};
				this.ws.send(JSON.stringify(payload));
				break;
			}
			case 'removed': {
				this.symbolsSet.delete(symbol);
				const payload = {
					method: 'UNSUBSCRIBE',
					params: [`${symbol}@ticker`],
					id: this.nonce++,
				};
				this.ws.send(JSON.stringify(payload));
				break;
			}
		}
	}
}
