module.exports = class CS5D041B08C61698495507576 {
	name = 'CS5D041B08C61698495507576';

	async up(queryRunner) {
		await queryRunner.query(
			`CREATE TABLE "tickers" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "price" character varying NOT NULL, "timestamp" TIMESTAMP NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "symbolId" uuid, CONSTRAINT "PK_c2a03993ab14bc2e244d7571f1c" PRIMARY KEY ("id"))`,
			undefined,
		);
		await queryRunner.query(`CREATE INDEX "IDX_d7a7f2d2caa8280555a42a4c02" ON "tickers" ("price", "timestamp") `, undefined);
		await queryRunner.query(
			`CREATE TABLE "symbols" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "symbol" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), CONSTRAINT "UQ_8537c94ae17acdcbd2cc15c99a0" UNIQUE ("symbol"), CONSTRAINT "PK_f9967bf9e35433b0a81ad95f8bf" PRIMARY KEY ("id"))`,
			undefined,
		);
		await queryRunner.query(
			`ALTER TABLE "tickers" ADD CONSTRAINT "FK_725b6699348af4c01369d921be2" FOREIGN KEY ("symbolId") REFERENCES "symbols"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
			undefined,
		);
		await queryRunner.query(`INSERT INTO "symbols" (symbol) VALUES ('btcusdt'), ('ethusdt')`, undefined);
	}

	async down(queryRunner) {
		await queryRunner.query(`ALTER TABLE "tickers" DROP CONSTRAINT "FK_725b6699348af4c01369d921be2"`, undefined);
		await queryRunner.query(`DROP TABLE "symbols"`, undefined);
		await queryRunner.query(`DROP INDEX "public"."IDX_d7a7f2d2caa8280555a42a4c02"`, undefined);
		await queryRunner.query(`DROP TABLE "tickers"`, undefined);
	}
};
