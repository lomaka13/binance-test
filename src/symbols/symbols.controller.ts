import { Post, Body, Delete, Param, Get } from '@nestjs/common';

import { ApiController } from '@binance-test/api';
import { ReturnTypeOfClassMethod } from '@binance-test/fixtures';

import { CreateTickerDto } from './dto';
import { SymbolsService } from './symbols.service';

@ApiController('symbols')
export class SymbolsController {
	constructor(private readonly service: SymbolsService) {}

	@Get()
	async list(): ReturnTypeOfClassMethod<SymbolsService, 'list'> {
		return this.service.list();
	}

	@Post()
	async addSymbol(@Body() { symbol }: CreateTickerDto): ReturnTypeOfClassMethod<SymbolsService, 'addSymbol'> {
		return this.service.addSymbol(symbol);
	}

	@Delete('/:symbol')
	async removeSymbol(@Param('symbol') symbol: string): ReturnTypeOfClassMethod<SymbolsService, 'removeSymbol'> {
		return this.service.removeSymbol(symbol);
	}
}
