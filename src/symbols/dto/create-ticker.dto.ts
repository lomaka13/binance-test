import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class CreateTickerDto {
	@ApiProperty()
	@IsString()
	@IsNotEmpty()
	symbol: string;
}
