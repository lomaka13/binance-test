import { Test, TestingModule } from '@nestjs/testing';

import { TypeOrmMock, getRepositoryToken } from '@binance-test/mocks/typeorm';
import { RedisService } from '@binance-test/redis';

import { SymbolsEntity } from '../entities';

import { SymbolsService } from './symbols.service';

describe('SymbolsService', () => {
	let service: SymbolsService;
	const symbolsRepository = new TypeOrmMock(SymbolsEntity).factory();

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [
				SymbolsService,
				{ provide: getRepositoryToken(SymbolsEntity), useValue: symbolsRepository },
				{ provide: RedisService, useValue: jest.fn() },
			],
		}).compile();

		service = module.get<SymbolsService>(SymbolsService);
	});

	it('should be defined', () => {
		expect(service).toBeDefined();
	});
});
